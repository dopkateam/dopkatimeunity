﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EshkaController : Bullet   
{
    public float rotationSpeed = 180f;
    public float rotationAcceleration = 3f;

    public override void Fly()
    {
        rb.angularVelocity = rotationSpeed;
    }

    public override void HitEnemy(GameObject obj)
    {
        throw new System.NotImplementedException();
    }

    public override void HitObj(GameObject obj)
    {
        throw new System.NotImplementedException();
    }
}
