﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public float bulletspeed;
    public int damage;
    public Rigidbody2D rb;
    void Start()
    {
        rb.velocity = transform.right * bulletspeed;
        Fly();
    }
    public void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.TryGetComponent(out Enemy enemy))
        {
            HitEnemy(hitInfo.gameObject);
        }
        else HitObj(hitInfo.gameObject);
    }
    abstract public void Fly();
    abstract public void HitEnemy(GameObject obj);
    abstract public void HitObj(GameObject obj);
}
