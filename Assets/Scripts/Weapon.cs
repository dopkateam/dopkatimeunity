﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject Bulllet;
    public GameObject Ult;
    public Transform BulletPosition;
    public float FireRate;
    public static int ClipSize;
    public float ReloadSpeed;


    private int clip = ClipSize;
    public void Shoot()
    {
        if (clip == 0)
        {
            Reload();
        }
        
        Instantiate(Bulllet, BulletPosition.position, BulletPosition.rotation);
        clip--;
        StartCoroutine(Ratefire());
    }

    public void Ultimativa()
    {
        AbstractUltimativa UseUlt = Ult.GetComponent<AbstractUltimativa>();
        UseUlt.Position = BulletPosition;
        UseUlt.UseUltimativa();
    }

    private void Reload()
    {
        StartCoroutine(ReloadTime());
        clip = ClipSize;
    }

    IEnumerator Ratefire()
    {
        yield return new WaitForSeconds(1/FireRate);
    }

    IEnumerator ReloadTime()
    {
        yield return new WaitForSeconds(ReloadSpeed);
    }

    public void IsPicked()
    {
        Destroy(gameObject);
    }
}
