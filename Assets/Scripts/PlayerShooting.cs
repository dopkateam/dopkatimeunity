﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject wep;
    private bool Isattacking = false;
    private bool IsUsingUlt = false;
    private Weapon weapon;

   
    void Start()
    {
        weapon = wep.GetComponent<Weapon>();
    }

    void Update()
    {
        Isattacking = Input.GetButtonDown("Fire1");
        IsUsingUlt = Input.GetButtonDown("Fire2");
        if (Isattacking)
        {
            weapon.Shoot();   
        }
        
        if(IsUsingUlt)
        {
            weapon.Ultimativa();
        }

    }
    public void Thisweapon(GameObject weapon)
    {
        wep = weapon;

    }

}
