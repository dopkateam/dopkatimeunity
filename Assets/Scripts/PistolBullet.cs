﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolBullet : Bullet
{
   
    public override void Fly()
    {
        
    }
    public override void HitEnemy(GameObject obj)
    {
        Enemy enemy = obj.GetComponent<Enemy>();
        enemy.HitPoints -= damage;
        Destroy(obj);
    }

    public override void HitObj(GameObject obj)
    {
        Destroy(obj);
    }


}
