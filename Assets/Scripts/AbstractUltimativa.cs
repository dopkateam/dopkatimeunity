﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractUltimativa : MonoBehaviour
{
    public Transform Position;
    public abstract void UseUltimativa();
}
