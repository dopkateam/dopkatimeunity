﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemControl : MonoBehaviour
{
    GameObject Player;
    Transform hands;


    void Start()
    {
        hands.position = new Vector3(Player.GetComponent<Transform>().position.x + 1f, Player.GetComponent<Transform>().position.y + 0.3f);
    }

    public void OnTriggerEnter2D(Collider2D hitInfo)
    {
       if (hitInfo.TryGetComponent(out Weapon wep))
        {
            Pickup(wep.gameObject);
        }
    }

    private void Pickup(GameObject obj)
    {
        obj.GetComponent<Weapon>().IsPicked();
        Player.GetComponent<PlayerShooting>().Thisweapon(obj);
        Instantiate(obj, hands);
    }
}
