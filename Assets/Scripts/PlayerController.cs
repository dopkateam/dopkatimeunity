﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxSpeedHor = 10f;
    public float maxSpeedVer = 10f;
    public bool IsFacingRight = true;

    private float horizontalMove;
    private float verticalMove;

    void Start()
    {
        //Flip();
    }

    private void Update()
    {

        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");
    }

    void FixedUpdate()
    {
        //float diagonalMove = Mathf.Sqrt(Mathf.Pow(verticalMove, 2) + Mathf.Pow(horizontalMove, 2));
        GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalMove * maxSpeedHor, verticalMove * maxSpeedVer);
        if (horizontalMove > 0 && !IsFacingRight)
        {
            Flip();
        }
        else if (horizontalMove < 0 && IsFacingRight)
        {
            Flip();
        }
    }

    public void Flip()
    {

        IsFacingRight = !IsFacingRight;

        transform.Rotate(0f, 180f, 0f);
    }
}

