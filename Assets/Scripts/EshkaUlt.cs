﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EshkaUlt : AbstractUltimativa
{
    public GameObject Ashka;
    public float rotationSpeed;
    public float bulletspeed;
    public int damage;
    public Rigidbody2D rb;

    public void Awake()
    {
        Position = Ashka.GetComponent<Transform>();
    }

    public void Start()
    {
        rb.velocity = transform.right * bulletspeed;
        Fly();
    }
    public void FixedUpdate()
    {
        transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
        //transform.Rotate(0f, 0f, rotationSpeed);
    }
    public void Fly()
    {
        //rb.velocity = transform.right * bulletspeed;       
        rb.angularVelocity = rotationSpeed;
    }
    public override void UseUltimativa()
    {
        Instantiate(Ashka, Position.position, Position.rotation);
        //bulletspeed = 5;
        //rotationSpeed = 360;
    }
}
