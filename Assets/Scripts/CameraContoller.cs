﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraContoller : MonoBehaviour
{
    public Component t;
    public Transform target;
    public float smooth = 3f;
    public Vector3 offset;
    private Vector3 previousPosition;
    private int previousPositionCounter = 0;
    private Vector3 deltaPosition = new Vector3(0f, 0f, 0f);
    public int previousPositionDelay = 100;

    void FixedUpdate()
    {
        // пытался сделать отдаление камеры при передвижении игрока, но пока что не получается
        Vector3 deltaPosition = 100*(target.position - previousPosition);
        Vector3 deltaPositionZed = new Vector3(0f, 0f, Mathf.Abs(deltaPosition.z));
        Debug.Log(deltaPositionZed);

        // плавное передвижение камеры относительно игрока
        Vector3 desiredPosition = target.position + offset + deltaPositionZed;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smooth);
        transform.position = smoothedPosition;

        // иногда мы обновляем разницу передвижения
        if (previousPositionCounter == 0)
        {
            previousPosition = target.position;
        }
        previousPositionCounter = (previousPositionCounter + 1) % previousPositionDelay;

        //transform.LookAt(target);
    }
}
